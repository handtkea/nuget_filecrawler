﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCrawler
{
    public class FileCrawler
    {

        // public methods
        public async Task<List<FileInfo>> GetFilesByExtensionList(string folder, string[] extensions, bool recursive)
        {

            List<FileInfo> mFileInfos = new List<FileInfo>();

            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(folder);

                if (recursive)
                {
                    mFileInfos = dirInfo.EnumerateFiles("*.*", SearchOption.AllDirectories)
                        .Where(f => extensions.Contains(f.Extension.ToLower()))
                        .ToList<FileInfo>();
                }
                else
                {
                    mFileInfos = dirInfo.EnumerateFiles("*.*")
                        .Where(f => extensions.Contains(f.Extension.ToLower()))
                        .ToList<FileInfo>();
                }
                

                return mFileInfos;
            }
            catch
            {
                return mFileInfos;
            }

        }

    }


}
